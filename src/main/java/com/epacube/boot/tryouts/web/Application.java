package com.epacube.boot.tryouts.web;

import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.context.request.WebRequest;

import java.util.*;

@Controller
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
public class Application {

    Random r = new Random();

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "<h1>Welcome To my application!</h1><p>Lets try to impress some people!<br/> Lets Get Some <a href='/bootstrap'>Info!</a><br/>Lets Go <a href='/somewhere'>Somewhere else!</a><p>";
    }

    public String printInfo(){
        return "some data here";
    }

    @RequestMapping("/out")
    @ResponseBody
    String out() {
        return "OK, we're out, now what?<br/> " +
                "Say <a href='/greeting'>Hi</a> to some friends, or<br/>"+
                "lets go get some <a href='/stuff'>Stuff!</a>,<br/>"+
                "Click <a href='/bootstrap'>here</a> for information about EpaCube!";
    }

    @RequestMapping("/somewhere")
    @ResponseBody
    String somewhere() {
        return "Something else to go to<br/>" +
                "you can display your <a href='/greeting'>greeting</a>, or<br/>" +
                "view the fibonacci sequence up to 32 <a href='/fib'>here</a>";
    }

    @RequestMapping("/fib")
    @ResponseBody
    String fib(){
        int numbers = 33;
        if(numbers > 1){
            int[] fibNums = new int[numbers];
            String returnString;
            fibNums[0] = 1;
            fibNums[1] = 1;
            returnString = fibNums[0] + ", " + fibNums[1];
            for(int i = 2; i<fibNums.length; i++){
                fibNums[i] = fibNums[i-1] + fibNums[i-2];
                returnString+=", " + fibNums[i];
            }
            return returnString;
        }
        else if(numbers == 1) return "1";
        else return "";

    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/bootstrap")
    public String greeting(WebRequest wr, Model model, @RequestParam(name="name", required=false, defaultValue="World") String name) {
        model.addAttribute("parameters", wr.getParameterMap());
        model.addAttribute("name", name);
        return "bootstrap";
    }

    ResponseEntity<Map<String,Object>> buildMessage(String message){
        Map<String,Object> m = new LinkedHashMap<>();
        m.put("message",message);
        return new ResponseEntity<>(m, HttpStatus.OK);
    }

    @RequestMapping("/stuff")
    @ResponseBody
    ResponseEntity<Map<String,Object>> stuff() {
        Map<String,Object> m = new LinkedHashMap<>();
        List<Map<String,Object>> l = new ArrayList<Map<String,Object>>();
        for(int i=0;i<10;i++){
            Map<String,Object> im = new LinkedHashMap<>();
            int x = r.nextInt(5000);
            for(String s :"id,name,code,display".split(",")){
                im.put(s,String.format("%s_%d",s,x));
            }
            l.add(im);
        }
        m.put("success",Boolean.TRUE);
        m.put("results",l);
        return new ResponseEntity<>(m, HttpStatus.OK);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}